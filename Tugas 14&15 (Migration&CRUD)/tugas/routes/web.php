<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.welcome');
// });

use App\Http\Controllers\CastController;

Route::get('/', function () {
    return view('pages.dashboard');
});


Route::get('/table', function () {
    return view('pages.table');
});

Route::get('/data-table', function () {
    return view('pages.dtable');
});

Route::get('/cast', [CastController::class, 'index'])->name('cast.index');

Route::get('/cast/create', [CastController::class, 'create'])->name('cast.create');

Route::post('/cast', [CastController::class, 'store'])->name('cast.store');

Route::get('/cast/{id}', [CastController::class, 'show'])->name('cast.show');

Route::get('/cast/{id}/edit', [CastController::class, 'edit'])->name('cast.edit');

Route::put('/cast/{id}', [CastController::class, 'update'])->name('cast.update');

Route::delete('/cast/{id}', [CastController::class, 'destroy'])->name('cast.destroy');