<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('pages.cast.index', compact('casts'));
    }

    public function create()
    {
        return view('pages.cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio' => 'nullable|string',
        ]);

        DB::table('casts')->insert([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);

        return redirect()->route('cast.index')->with('success', 'Cast created successfully!');
    }

    public function show($id)
    {
        $cast = DB::table('casts')->find($id);
        return view('pages.cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('casts')->find($id);
        return view('pages.cast.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio' => 'nullable|string',
        ]);

        DB::table('casts')->where('id', $id)->update([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);

        return redirect()->route('cast.index')->with('success', 'Cast updated successfully!');
    }

    public function destroy($id)
    {
        // Find the cast by ID
        $cast = DB::table('casts')->find($id);
    
        // Check if the cast exists
        if (!$cast) {
            return redirect()->route('cast.index')->with('error', 'Cast not found!');
        }
    
        // Delete the cast
        DB::table('casts')->where('id', $id)->delete();
    
        return redirect()->route('cast.index')->with('success', 'Cast deleted successfully!');
    }
}
