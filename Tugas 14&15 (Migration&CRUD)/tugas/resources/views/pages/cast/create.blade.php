@extends('layout.master')

@section('content')
    <h1>Create New Cast</h1>

    <form action="{{ route('cast.store') }}" method="POST">
        @csrf

        <label for="nama">Name:</label>
        <input type="text" id="nama" name="nama" required><br>

        <label for="umur">Age:</label>
        <input type="number" id="umur" name="umur" min="0" required><br>

        <label for="bio">Bio:</label>
        <textarea id="bio" name="bio"></textarea><br>

        <button type="submit">Create</button>
    </form>
@endsection
