@extends('layout.master')

@section('judul', 'Casts')

@section('content')
    <div class="row mb-3">
        <div class="col">
            <a href="{{ route('cast.create') }}" class="btn btn-primary">Add New Cast</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($casts as $key => $cast)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $cast->nama }}</td>
                    <td>
                        <form action="{{ route('cast.destroy', $cast->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{ route('cast.show', $cast->id) }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="{{ route('cast.edit', $cast->id) }}" class="btn btn-warning btn-sm">Update</a>
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3">No casts found.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
