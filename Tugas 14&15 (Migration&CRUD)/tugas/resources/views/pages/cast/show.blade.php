@extends('layout.master')

@section('judul', 'Cast Details')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Cast Details</h5>
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Name:</strong> {{ $cast->nama }}</li>
                <li class="list-group-item"><strong>Age:</strong> {{ $cast->umur }}</li>
                <li class="list-group-item"><strong>Bio:</strong> {{ $cast->bio }}</li>
            </ul>
        </div>
        <div class="card-footer">
            <a href="{{ route('cast.index') }}" class="btn btn-secondary">Back</a>
        </div>
    </div>
@endsection
