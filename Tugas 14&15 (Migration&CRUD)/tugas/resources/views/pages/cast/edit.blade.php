@extends('layout.master')

@section('content')
    <h1>Edit Cast</h1>

    @if ($cast)
        <form action="{{ route('cast.update', $cast->id) }}" method="POST">
            @csrf
            @method('PUT')

            <label for="nama">Name:</label>
            <input type="text" id="nama" name="nama" value="{{ $cast->nama }}" required><br>

            <label for="umur">Age:</label>
            <input type="number" id="umur" name="umur" value="{{ $cast->umur }}" required><br>

            <label for="bio">Bio:</label>
            <textarea id="bio" name="bio">{{ $cast->bio }}</textarea><br>

            <button type="submit">Update</button>
        </form>
    @else
        <p>Cast not found.</p>
    @endif
@endsection
