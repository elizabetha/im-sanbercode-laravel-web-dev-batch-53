<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar (){
        return view("register");
    }

    public function name (request $request){
        $fname = $request['fn'];
        $lname = $request['ln'];

        return view("welcome", ['fname'=> $fname,'lname'=> $lname]);
    }
}
